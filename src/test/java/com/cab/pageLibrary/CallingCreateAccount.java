package com.cab.pageLibrary;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.cab.AppModule.createAccount;

public class CallingCreateAccount {
	
	static WebDriver driver;

	@BeforeTest
	public static void launchApp() throws IOException{
		
		driver=SetUp.init();
		
		
	}

@Test

public static void loginApp() throws IOException, Exception{
	createAccount.createAccountMethod(driver);
	
}

@AfterTest

public static void closeBrowser(){
	driver.quit();
	
}
}
