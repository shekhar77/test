package com.cab.pageLibrary;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.cab.Utility.UtilityMethod;

public class SetUp {
public static WebDriver driver;

public static WebDriver init() throws IOException{
	
	String browser=UtilityMethod.getKeyValueFromConfig("browser");
	
	if(browser.equalsIgnoreCase("firefox")){
		driver=new FirefoxDriver();
		
	}
	else if(browser.equalsIgnoreCase("chrome"))
	{
		System.setProperty("webdriver.chrome.driver","D:\\UBER\\CabAccount\\src\\test\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
	}
	driver.manage().window().maximize();
	driver.get(UtilityMethod.getKeyValueFromConfig("url"));
	return driver;
	
}

}
