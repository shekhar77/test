package com.cab.constant;

public class ConstantVar {

public static final String confFile=System.getProperty("user.dir")+"\\src\\test\\resources\\config.properties";
public static final String excelFile=System.getProperty("user.dir")+"\\src\\test\\resources\\data.xlsx";
public static final String allLocatorsFile=System.getProperty("user.dir")+"\\src\\test\\resources\\allLocators.properties";

}
