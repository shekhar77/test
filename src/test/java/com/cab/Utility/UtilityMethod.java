package com.cab.Utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.cab.constant.ConstantVar;

public class UtilityMethod {
	
	
	static FileInputStream file;
	static Properties repo=new Properties();;
	WebDriver driver;
	static WebElement loct;
	static XSSFCell celldata=null;
	static String value;
	public static WebElement getLocator(WebDriver driver,String key) throws IOException{
		
		
		try {
			String keyValue=UtilityMethod.getKeyValue(key);
			String locator[]=keyValue.split("##");
			String locatorValue=locator[0];
			String locatorType=locator[1];
			
			if(locatorType.equalsIgnoreCase("xp")){
				loct=driver.findElement(By.xpath(locatorValue));
				
			}else if(locatorType.equalsIgnoreCase("id"))
			{	
				
				loct=driver.findElement(By.id(locatorValue));
				
				
			}else{
				System.out.println("locator not found");
			}
			return loct;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			
		}
		return loct;
		
		
	}
	
	
	public static String getKeyValue(String key) throws IOException{
		
          file = new FileInputStream(ConstantVar.allLocatorsFile);
         repo.load(file);
         String value=repo.getProperty(key);
		  
		return value;
		
		
		
	}
	public static String getKeyValueFromConfig(String key) throws IOException{
		
        file = new FileInputStream(ConstantVar.confFile);
       repo.load(file);
       String value=repo.getProperty(key);
		  
		return value;
		
		
		
	}
	
	public static String getCellData(int row,int coulumn,XSSFSheet sheet){
		
		XSSFCell cellValue=sheet.getRow(row).getCell(coulumn);
		/*f (celldata == null)
		{
		   System.out.println("Cell is Empty in Column:" + cols);

		}*/
		if(cellValue.getCellType()==XSSFCell.CELL_TYPE_NUMERIC){
			
		value=NumberToTextConverter.toText(cellValue.getNumericCellValue());
			
		}
		else{
				
		value=cellValue.getStringCellValue();	
			
		}
		return value;
		}
		

	
	

}
