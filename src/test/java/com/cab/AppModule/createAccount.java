package com.cab.AppModule;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import com.cab.Utility.UtilityMethod;
import com.cab.constant.ConstantVar;

public class createAccount {
 static WebDriver driver;
 static File f;
 static FileInputStream file;
 static WebElement firstname;
 static WebElement lastname;
 static WebElement emailId;
 static WebElement phone;
 static WebElement password;
 static WebElement city;
 static WebElement submitButton;
 static WebElement citydropDown; 
 static WebDriverWait wait;
@Test
public static void createAccountMethod(WebDriver driver) throws IOException, Exception{
  
wait = new WebDriverWait(driver, 10);
 f=new File(ConstantVar.excelFile);
 file=new FileInputStream(f);

 XSSFWorkbook wb=new XSSFWorkbook(file);
 XSSFSheet sheet=wb.getSheet("Sheet1");
 int totalRows=sheet.getPhysicalNumberOfRows();
 for(int i=0;i<totalRows;i++){
	 try {
		firstname=UtilityMethod.getLocator(driver, "firstname");
		 firstname.sendKeys(UtilityMethod.getKeyValueFromConfig("firstname"));
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
	 try {
		lastname=UtilityMethod.getLocator(driver, "lastname");
		 lastname.sendKeys(UtilityMethod.getKeyValueFromConfig("lastname"));
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
	try {
		emailId= UtilityMethod.getLocator(driver, "emailId");
		emailId.sendKeys(UtilityMethod.getCellData(i, 0, sheet));
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	try {
		phone=UtilityMethod.getLocator(driver, "phone");
		phone.sendKeys(UtilityMethod.getCellData(i, 1, sheet));
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	try {
		password=UtilityMethod.getLocator(driver, "password");
		 password.sendKeys(UtilityMethod.getKeyValueFromConfig("password"));
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	try {
		city=UtilityMethod.getLocator(driver, "city");
		city.sendKeys(UtilityMethod.getKeyValueFromConfig("city"));
		Actions act = new Actions(driver);
		Thread.sleep(1000);
		WebElement text = driver.findElement(By.xpath("//div[contains(text(),'Delhi, India')]"));
		//WebElement ci = UtilityMethod.getLocator(driver, "city");
		Thread.sleep(2000);
		act.moveToElement(text).click().build().perform();
		Thread.sleep(2000);
		/*try{
		city.findElement(By.xpath("//div[contains(text(),'Delhi, India')]")).click();
		}catch(ElementNotVisibleException e)
		{
			driver.findElement(By.xpath("//div[contains(text(),'Delhi, India')]")).click();
		}
		*/
		/*citydropDown=UtilityMethod.getLocator(driver, "citydropDown");
		
		wait.until(ExpectedConditions.visibilityOf(UtilityMethod.getLocator(driver, "citydropDown")));
		citydropDown.click();*/
	
	   
       
		/*List<WebElement> optionsToSelect = driver.findElements(By.xpath(".//*[@id='app-content']/div/div[1]/div[1]/div[2]/div/div/form/div[9]/div/div/div[2]"));
		
	    
		
		for(WebElement option : optionsToSelect){
			 System.out.println(option.getText());
	        if(option.getText().equals("Delhi, India")) {
	           // System.out.println("Trying to select: "+textToSelect);
	            option.click();
	            break;
	        }
		
	    
	    }
		*/
		
		
		
		
		
		
		
		/*citydropDown=UtilityMethod.getLocator(driver, "citydropDown");
        
        citydropDown.click();*/
    

/*Select oSelect = new Select(city);
 
oSelect.selectByVisibleText(UtilityMethod.getKeyValueFromConfig("city"));*/
		 
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	submitButton=UtilityMethod.getLocator(driver, "submitButton");
	submitButton.click();
	
	Thread.sleep(5000); 
	driver.navigate().to("https://www.uber.com/a/join?join_v=oldicon&exp=61208t1");

 }
	
}

}
